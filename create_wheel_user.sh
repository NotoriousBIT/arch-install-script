#!/bin/sh
read -p "Specify the username: " username
useradd -m $username
usermod -aG wheel,audio,video,power $username

passwd $username

pacman -S --noconfirm sudo

sed -i "s|# %wheel ALL=(ALL:ALL) ALL|%wheel ALL=(ALL:ALL) ALL|" /etc/sudoers