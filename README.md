# How to install my Arch-Linux
## Step 1: Download the latest image
https://www.archlinux.org/download/

## Step 2: Copy the image to a USB Stick
`$ dd if=/path/to/*.iso of=/dev/XXX bs=64k status=progress`

## Step 3: Boot into the live environment
For changing the keyboad layout to germen type in<br>
`$ loadkeys de-latin1`

>**INFO**
"-" = "?" on english keyboard

## Step 4: Download and run the install script
`$ curl https://gitlab.com/NotoriousBIT/arch-install-script/-/raw/main/install.sh -L -o install.sh` <br>
`$ sh install.sh`

>**NOTE**
Follow the instructions
## Step 6: Reboot into installation

## Step 7 (optional): Install the environment including Dotfiles
`$ sudo cp /install_env.sh ~/install_environment.sh` <br>
`$ sudo chown <username>: ~/install_environment.sh` <br>
`$ sh install_environment.sh`

# Thanks to:
 - https://gist.github.com/mattiaslundberg/8620837
