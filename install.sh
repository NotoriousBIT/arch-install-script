#!/bin/sh
echo "# Setting keyboard layout"
loadkeys de-latin1

echo "# Setting system time"
timedatectl set-ntp true

echo "# lsblk"
lsblk

while true; do
  read -p "Specify the name of the device to install to (i.e. /dev/sda) " install_device
  read -p "You selcted \"$install_device\", is this correct? [Y/n] " yn
  case $yn in
          [Yy][eE][sS]|[yY] ) break;;
          [Nn][Oo]|[nN] ) ;;
          * ) echo "Please answer yes or no.";;
  esac
done

echo "# Unmount everything"
umount -R /mnt

# Create partitions
# 1 500MB EFI
# 2 500MB Boot
# 3 100% main

# Get the corrosponding numbers of fdisk partition types
EFI_SYSTEM_NUMBER=$(echo "l" | fdisk $install_device | grep -oP "[0-9]+(?= EFI System )")
LINUX_FILESYSTEM_NUMBER=$(echo "l" | fdisk $install_device | grep -oP "[0-9]+(?= Linux filesystem )")
LINUX_LVM_NUMBER=$(echo "l" | fdisk $install_device | grep -oP "[0-9]+(?= Linux LVM)")

echo "EFI_SYSTEM_NUMBER $EFI_SYSTEM_NUMBER"
echo "LINUX_FILESYSTEM_NUMBER $LINUX_FILESYSTEM_NUMBER"
echo "LINUX_LVM_NUMBER $LINUX_LVM_NUMBER"

# Wipe the disk
echo "# Wipe the disk"
wipefs -af $install_device

echo "# Create a new empty GPT partition table"
(echo g; echo w) | fdisk $install_device

# Create partition 1
echo "# Create first partition (EFI 500M)"
(echo n; echo 1; echo ""; echo "+500M"; echo w) | fdisk $install_device
(echo t; echo $EFI_SYSTEM_NUMBER; echo w) | fdisk $install_device

# Create partition 2
echo "# Create first partition (BOOT 500M)"
(echo n; echo 2; echo ""; echo "+500M"; echo w) | fdisk $install_device
(echo t; echo 2; echo $LINUX_FILESYSTEM_NUMBER; echo w) | fdisk $install_device

# Create partition 3
echo "# Create first partition (LVM 100%FREE)"
(echo n; echo 3; echo ""; echo ""; echo w) | fdisk $install_device
(echo t; echo 3; echo $LINUX_LVM_NUMBER; echo w) | fdisk $install_device

# Get partition names
PARTITION_EFI=$(fdisk -l $install_device | grep "500M EFI System" | grep -oP "^[a-z0-9\/]+")
echo "EFI partition is @ $PARTITION_EFI"

PARTITION_BOOT=$(fdisk -l $install_device | grep "500M Linux filesystem" | grep -oP "^[a-z0-9\/]+")
echo "Boot partition is @ $PARTITION_BOOT"

PARTITION_LVM=$(fdisk -l $install_device | grep "Linux LVM" | grep -oP "^[a-z0-9\/]+")
echo "LVM partition is @ $PARTITION_LVM"

# Format the EFI partition
echo "# Format the EFI partition"
mkfs.fat -n EFI -F32 $PARTITION_EFI

# Format the boot partition
echo "# Format the boot partition"
mkfs.ext4 -FL BOOT $PARTITION_BOOT

# Set up encryption
echo "# Format encypted partition"
cryptsetup luksFormat $PARTITION_LVM

echo "# Open encypted partition"
cryptsetup open --type luks $PARTITION_LVM lvm

echo "# Initializing volume"
pvcreate --dataalignment 1m /dev/mapper/lvm

echo "# Create volume group"
vgcreate vg0 /dev/mapper/lvm

echo "# Create logical volume (lv_root)"
lvcreate -l 100%FREE vg0 -n lv_root

echo "# Format logical volume (lv_root)"
mkfs.ext4 -FL root /dev/vg0/lv_root

echo "# Mount logical volume (lv_root)"
mount /dev/vg0/lv_root /mnt

echo "# Create boot dir"
mkdir /mnt/boot

echo "# Mount boot partition"
mount $PARTITION_BOOT /mnt/boot

echo "# Create etc dir"
mkdir /mnt/etc

echo "# Generate fstab"
genfstab -U -p /mnt >> /mnt/etc/fstab


while true; do
  cat /mnt/etc/fstab
  read -p "Are all the partition looking correct? [Y/n] " yn
  case $yn in
          [Yy][eE][sS]|[yY] ) break;;
          [Nn][Oo]|[nN] ) exit;;
          * ) echo "Please answer yes or no.";;
  esac
done

echo " Installing packages"
pacstrap /mnt base base-devel linux-lts linux-firmware linux-lts-headers

echo " chroot"
curl -L https://gitlab.com/NotoriousBIT/arch-install-script/-/raw/main/install_chroot.sh -o /mnt/install_chroot.sh
arch-chroot /mnt sh install_chroot.sh

echo "# Create wheel user"
curl -L https://gitlab.com/NotoriousBIT/arch-install-script/-/raw/main/create_wheel_user.sh -o /mnt/create_wheel_user.sh
arch-chroot /mnt sh create_wheel_user.sh

echo "# Download Env Install Script"
curl -L https://gitlab.com/NotoriousBIT/dotfiles/-/raw/main/install.sh -o /mnt/install_env.sh

while true; do
  read -p "Do you wish to reboot? [Y/n] " yn
  case $yn in
          [Yy][eE][sS]|[yY] ) umount -a; reboot; break;;
          [Nn][Oo]|[nN] ) break;;
          * ) echo "Please answer yes or no.";;
  esac
done
